package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import org.fasttrackit.pages.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.fasttrackit.pages.DataSource.HOME_PAGE;
import static org.testng.Assert.*;

public class CartPageTest {

    BasePage basePage;
    HomePage homePage;
    Header header;
    Footer footer;
    LoginForm loginForm;
    CartPage cartPage;

    @BeforeTest
    public void openPage() {
        Selenide.open(HOME_PAGE);
        basePage = new BasePage();
        homePage = new HomePage();
        header = new Header();
        footer = new Footer();
        loginForm = new LoginForm();
        cartPage = new CartPage();
    }

    @AfterMethod
    public void cleanUp() {
        homePage.resetApp();
    }

    @Test
    public void verify_cart_page_title() {
        homePage.openCartPage();
        assertTrue(cartPage.isCartPageTitleDisplayed(), "Cart page title is displayed.");
        assertEquals(cartPage.cartPageName(),"Your cart", "Cart page title is 'Your cart'.");
    }

    @Test
    public void verify_cart_page_message() {
        homePage.openCartPage();
        assertTrue(cartPage.isCartPageMessageDisplayed(), "cart page message is displayed.");
        assertEquals(cartPage.cartPageMessage(), "How about adding some products in your cart?",
                "Cart page message is 'How about adding some products in your cart?'");
    }

    @Test
    public void clicking_on_continue_shopping_button_redirects_to_product_page() {
        homePage.addProductsToBasketAndOpenCartPage();
        assertTrue(cartPage.isContinueShoppingButtonDisplayed(), "Continue shopping button is displayed.");
        assertEquals(cartPage.continueShoppingButtonRedirectUrl(), HOME_PAGE + "#/products",
                "Clicking on continue shopping button redirects to products page.");
    }
}
