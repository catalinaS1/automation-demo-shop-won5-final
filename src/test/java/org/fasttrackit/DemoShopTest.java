package org.fasttrackit;

import io.qameta.allure.selenide.AllureSelenide;
import org.fasttrackit.pages.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.logevents.SelenideLogger.addListener;
import static org.fasttrackit.pages.DataSource.*;
import static org.testng.Assert.*;


public class DemoShopTest {
    BasePage basePage;
    HomePage homePage;
    Header header;
    Footer footer;
    LoginForm loginForm;
    CartPage cartPage;

    @BeforeClass
    public void before() {
        AllureSelenide listener = new AllureSelenide().screenshots(true).savePageSource(true).includeSelenideSteps(true);
        addListener("AllureSelenide", listener);
        open(HOME_PAGE);

        basePage = new BasePage();
        homePage = new HomePage();
        header = new Header();
        footer = new Footer();
        loginForm = new LoginForm();
        cartPage = new CartPage();
    }

    @AfterMethod
    public void cleanUp() {
        basePage.resetApp();
    }

    @Test
    public void is_cart_badge_displayed() {
        AWESOME_GRANITE_CHIPS_PRODUCT.addToBasket();
        assertTrue(header.isCartBadgeIconDisplayed(), "After adding a product to cart, shopping cart badge is visible.");
    }

    @Test
    public void cart_icon_updates_upon_adding_a_product_to_the_cart() {
        AWESOME_GRANITE_CHIPS_PRODUCT.addToBasket();
        assertEquals(header.productsInCartBadge(), "1", "After adding a product to basket, the cart is updated.");
    }

    @Test
    public void favorites_icon_crashes_upon_adding_a_product_to_favorites() {
        AWESOME_METAL_CHAIR_PRODUCT.addToWishlist();
        assertTrue(AWESOME_METAL_CHAIR_PRODUCT.isInWishList(),
                "After adding a product to favorites, the product favorites icon crashes.");
    }

    @Test
    public void favorites_icon_updates_upon_adding_a_product_to_favorites() {
        AWESOME_METAL_CHAIR_PRODUCT.addToWishlist();
        assertEquals(header.productsInFavoritesBadge(), "1",
                "After adding a product to favorites, the favorites icon badge is updated.");
    }

    @Test(dataProvider = "dataSource", dataProviderClass = DataSource.class)
    public void verify_products_on_homepage(Product product) {
        assertTrue(product.isDisplayed(), "Product" + product.getProductId() + "is displayed on page.");
        assertEquals(product.url(), product.getExpectedUrl(), "Product" + product.getProductId() + "page is opened.");
        assertEquals(product.getPrice(), product.getExpectedPrice(),
                "Product" + product.getProductId() + "price is" + product.getExpectedPrice());
    }

    @Test
    public void clicking_on_question_button_opens_help_modal() {
        assertTrue(footer.isQuestionIconDisplayed(), "Question icon is displayed on page.");
        footer.openHelpModal();
        assertTrue(footer.isHelpModalDisplayed(), "After clicking the question icon, help modal is displayed.");
//        footer.closeModal();
   }

    @Test
    public void verify_help_modal_title() {
        footer.openHelpModal();
        assertTrue(footer.isModalTitleDisplayed(), "Modal title is displayed.");
        assertEquals(footer.helpModalTitle(),"Help", "Modal title is Help.");
//        footer.closeModal();
    }
}
