package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import org.fasttrackit.pages.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.fasttrackit.pages.DataSource.HOME_PAGE;
import static org.testng.Assert.*;
public class LoginTest {

    BasePage basePage;
    HomePage homePage;
    Header header;
    Footer footer;
    LoginForm loginForm;
    CartPage cartPage;

    @BeforeTest
    public void openPage() {
        Selenide.open(HOME_PAGE);
        basePage = new BasePage();
        homePage = new HomePage();
        header = new Header();
        footer = new Footer();
        loginForm = new LoginForm();
        cartPage = new CartPage();
    }

    @AfterMethod
    public void cleanUp() {
        homePage.resetApp();
    }

    @Test
    public void verify_username_field() {
        header.openLoginModal();
        assertTrue(loginForm.isUserNameFieldDisplayed(), "After the login modal is opened, user name field is displayed. ");
    }

    @Test
    public void verify_password_field() {
        header.openLoginModal();
        assertTrue(loginForm.isPasswordFieldDisplayed(), "After the login modal is opened, password field is displayed.");
    }

    @Test
    public void verify_submit_button() {
        header.openLoginModal();
        assertTrue(loginForm.isSubmitButtonDisplayed(), "After the login modal is opened, submit button is displayed.");
    }

    @Test(dataProvider = "dataSource", dataProviderClass = DataSource.class)
    public void log_in_with_valid_credentials(Credentials user) {
        header.openLoginModal();
        loginForm.fillUserName(user.getUsername());
        loginForm.fillPassword(user.getPassword());
        loginForm.submitLogin();
        loginForm.resetApp();
    }

    @Test
    public void verify_logout_button() {
        header.openLoginModal();
        loginForm.fillUserName("dino");
        loginForm.fillPassword("choochoo");
        loginForm.submitLogin();
        assertTrue(loginForm.isLogoutButtonDisplayed(), "Logout button is displayed when a valid user is logged in.");
        loginForm.userLogout();
    }
}
