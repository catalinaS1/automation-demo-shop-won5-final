package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.pages.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.fasttrackit.pages.DataSource.HELLO_GUEST_GREETINGS_MSG;
import static org.fasttrackit.pages.DataSource.HOME_PAGE;
import static org.testng.Assert.*;

public class HomePageTest {

    BasePage basePage;
    HomePage homePage;
    Header header;
    Footer footer;
    LoginForm loginForm;
    CartPage cartPage;

    @BeforeTest
    public void openPage() {
        Selenide.open(HOME_PAGE);
        basePage = new BasePage();
        homePage = new HomePage();
        header = new Header();
        footer = new Footer();
        loginForm = new LoginForm();
        cartPage = new CartPage();
    }

    @AfterMethod
    public void cleanUp() {
        homePage.resetApp();
    }

    @Test
    public void verify_greeting_message_is_welcome() {
        assertTrue(homePage.isGreetingsMessageDisplayed(), "Welcome message is displayed.");
        assertEquals(homePage.greetingsMessage(), HELLO_GUEST_GREETINGS_MSG,"Greetings message is Hello guest!." );
    }

    @Test
    public void verify_homePage_logo() {
        assertTrue(homePage.isLogoDisplayed(), "Logo element is displayed in navigation bar.");
        assertEquals(homePage.logoRedirectUrl(), HOME_PAGE, "Clicking the Logo, redirects to homepage.");
    }

    @Test
    public void verify_home_page_title() {
        assertTrue(homePage.isHomePageTitleDisplayed(), "Home page title is displayed.");
        assertEquals(homePage.homePageTitle(), "Products", "Home page title is 'Products'.");
    }

    @Test
    public void verify_cart_icon() {
        assertTrue(homePage.isCartIconDisplayed(), "Cart icon is displayed.");
        assertEquals(homePage.cartRedirectUrl(), HOME_PAGE + "#/cart", "Page redirected to Cart page.");
    }

    @Test
    public void verify_wishList_icon() {
        assertTrue(homePage.isFavoritesIconDisplayed(), "Wishlist icon exists and is displayed.");
        assertEquals(homePage.wishListRedirectUrl(), HOME_PAGE + "#/wishlist",
                "Clicking the Favorites button redirects to Wishlist page.");
    }

    @Test
    public void clicking_on_login_button_opens_login_modal() {
        assertTrue(header.isLoginButtonDisplayed(), "Login button is displayed.");
        header.openLoginModal();
        assertTrue(header.isLoginModalDisplayed(), "After clicking the login button, login modal is displayed.");
//        header.closeModal();
    }

    @Test
    public void verify_search_form() {
        assertTrue(homePage.isSearchFormDisplayed(), "Search form is displayed on homepage.");
        assertTrue(homePage.isSearchButtonDisplayed(), "Search button is displayed.");
    }

    @Test(dataProvider = "dataSource", dataProviderClass = DataSource.class)
    public void verify_products_search(Product product) {
        homePage.fillSearchField(product.getTitle());
        homePage.clickOnSearchButton();
        assertEquals(product.url(), product.getExpectedUrl(), "Product" + product.getProductId() + "was found.");
        basePage.refresh();
    }

    @Test
    public void verify_sort_form() {
        assertTrue(homePage.isSortFormDisplayed(), "Sort form is displayed on home page.");
        homePage.clickOnSortFrom();
        assertTrue(homePage.isSortFormDisplayed(), "Cascading sort form is displayed.");
    }

    @Test
    public void verify_sort_mode_products() {
        ElementsCollection allProducts = $$(".card");
        SelenideElement firstProduct = allProducts.first();
        String firstProductTitle = firstProduct.getText();
        SelenideElement sortMode = $(".sort-products-select");
        assertTrue(sortMode.exists(), "Sort mode is displayed");
        sortMode.click();
        assertTrue(sortMode.isDisplayed(), "Sort type is displayed");
        SelenideElement highToLow = $("option[value=hilo]");
        highToLow.click();
        sortMode.click();
        ElementsCollection allProductsAfterSort = $$(".card");
        SelenideElement firsProductAfterSort = allProductsAfterSort.first();
        String fistProductTitleAfterSort = firsProductAfterSort.getText();
        assertNotEquals(firstProductTitle, fistProductTitleAfterSort);
    }
}
