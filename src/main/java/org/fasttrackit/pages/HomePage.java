package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static org.fasttrackit.pages.DataSource.AWESOME_METAL_CHAIR_PRODUCT;
import static org.fasttrackit.pages.DataSource.AWESOME_SOFT_SHIRT_PRODUCT;


public class HomePage extends BasePage {

    private final SelenideElement greetings = $(".navbar-text > span");
    private final SelenideElement logo = $(".navbar-brand .fa-shopping-bag");
    private final SelenideElement wishListIcon = $(".navbar-nav [data-icon=heart]");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement search = $("#input-search");
    private final SelenideElement searchButton = $(".btn-light");
    private final SelenideElement homePageName = $(".subheader-container");
    private final SelenideElement sortMode = $(".sort-products-select");

    /**
     * Page content
     */
    public String greetingsMessage() {
        return greetings.getText();
    }

    public String logoRedirectUrl() {
        return logo.parent().getAttribute("href");
    }

    public String wishListRedirectUrl() {
        return wishListIcon.parent().getAttribute("href");
    }

    public String cartRedirectUrl() {
        return cartIcon.parent().getAttribute("href");
    }

    public String homePageTitle() {
        return homePageName.getText();
    }


    /**
     * Verifiers
     */
    public boolean isGreetingsMessageDisplayed() {
        greetings.scrollIntoView(true);
        return greetings.exists() && greetings.isDisplayed();
    }

    public boolean isLogoDisplayed() {
        return logo.exists() && logo.isDisplayed();
    }

    public boolean isFavoritesIconDisplayed() {
        return wishListIcon.exists() && wishListIcon.isDisplayed();
    }

    public boolean isCartIconDisplayed() {
        return cartIcon.exists() && cartIcon.isDisplayed();
    }

    public boolean isSearchFormDisplayed() {
        return search.exists() && search.isDisplayed();
    }

    public boolean isSearchButtonDisplayed() {
        return searchButton.exists() && searchButton.isDisplayed();
    }

    public boolean isHomePageTitleDisplayed() {
        return homePageName.exists() && homePageName.isDisplayed();
    }

    public boolean isSortFormDisplayed() {
        return sortMode.exists() && sortMode.isDisplayed();
    }


    /**
     * Actions
     */
    public void fillSearchField(String lookingFor) {
        search.sendKeys(lookingFor);
    }

    public void clickOnSearchButton() {
        searchButton.click();
    }

    public void addProductsToBasketAndOpenCartPage() {
        AWESOME_METAL_CHAIR_PRODUCT.addToBasket();
        AWESOME_METAL_CHAIR_PRODUCT.addToBasket();
        cartIcon.click();
    }

    public void openCartPage() {
        cartIcon.click();
    }

    public void clickOnSortFrom() {
        sortMode.click();
    }

}
