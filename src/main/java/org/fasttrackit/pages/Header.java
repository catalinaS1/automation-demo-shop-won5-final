package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class Header extends BasePage {
    private final SelenideElement shoppingCartBadge = $("[data-icon=shopping-cart]~.shopping_cart_badge");
    private final SelenideElement favoritesIconBadge = $("[data-icon=heart]~.shopping_cart_badge");
    private final SelenideElement loginButton = $("[data-icon=sign-in-alt]");
    private final SelenideElement loginModal =$(".modal-content");

    /**
     * Page content
     */
    public String productsInCartBadge() {
        return shoppingCartBadge.getText();
    }

    public String productsInFavoritesBadge() {
        return favoritesIconBadge.getText();
    }

    /**
     * Verifiers
     */
    public boolean isCartBadgeIconDisplayed() {
        return shoppingCartBadge.exists() && shoppingCartBadge.isDisplayed();
    }

    public boolean isLoginButtonDisplayed() {
        return loginButton.exists() && loginButton.isDisplayed();
    }

    public boolean isLoginModalDisplayed() {
        return loginModal.exists() && loginModal.isDisplayed();
    }

    /**
     * Actions
     */
    public void openLoginModal() {
        loginButton.click();
    }
}
