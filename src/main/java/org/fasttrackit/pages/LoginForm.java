package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LoginForm extends BasePage {

    private final SelenideElement userNameField = $("[data-test=username]");
    private final SelenideElement passwordField = $("[data-test=password]");
    private final SelenideElement submit = userNameField.parent().$(".btn [data-icon=sign-in-alt]");
    private final SelenideElement logout = $("[data-icon=sign-out-alt]");

    /**
     * Verifiers
     */
    public boolean isUserNameFieldDisplayed() {
        return userNameField.exists() && userNameField.isDisplayed();
    }

    public boolean isPasswordFieldDisplayed() {
        return passwordField.exists() && passwordField.isDisplayed();
    }

    public boolean isSubmitButtonDisplayed() {
        return submit.exists() && submit.isDisplayed();
    }

    public boolean isLogoutButtonDisplayed() {
        return logout.exists() && logout.isDisplayed();
    }

    /**
     * Actions
     */
    public void fillUserName(String user) {
        userNameField.sendKeys(user);
    }
    public void fillPassword(String input) {
        passwordField.sendKeys(input);
    }

    public void submitLogin() {
        submit.click();
    }

    public void userLogout() {
        logout.click();
    }
}
