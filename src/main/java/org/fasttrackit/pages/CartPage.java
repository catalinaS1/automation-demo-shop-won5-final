package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage extends BasePage {


    private final SelenideElement continueShoppingButton = $("[href='#/products']");

    private final  SelenideElement cartPageTitle = $(".text-muted");
    private final SelenideElement cartMessage = $(".text-center");

    /**
     * Page content
     */
    public String continueShoppingButtonRedirectUrl() {
        return continueShoppingButton.getAttribute("href");
    }

    public String cartPageName() {
        return cartPageTitle.getText();
    }

    public String cartPageMessage() {
        return cartMessage.getText();
    }

    /**
     * Verifiers
     */
    public boolean isContinueShoppingButtonDisplayed(){
        return continueShoppingButton.exists() && continueShoppingButton.isDisplayed();
    }

    public boolean isCartPageTitleDisplayed() {
        return cartPageTitle.exists() && cartPageTitle.isDisplayed();
    }

    public boolean isCartPageMessageDisplayed() {
        return cartMessage.exists() && cartMessage.isDisplayed();
    }
}
