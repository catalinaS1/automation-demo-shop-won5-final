package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class Footer extends BasePage {
    private final SelenideElement questionIcon = $(".nav-item [data-icon=question]");
    private final SelenideElement helpModal = $(".modal-content");
    private final SelenideElement modalTitle = $(".modal-title");

    /**
     * Page content
     */
    public String helpModalTitle(){
        return modalTitle.getText();
    }

    /**
     * Verifiers
     */
    public boolean isQuestionIconDisplayed() {
        questionIcon.scrollIntoView(true);
        return questionIcon.exists() && questionIcon.isDisplayed();
    }

    public boolean isHelpModalDisplayed() {
        return helpModal.exists() && helpModal.isDisplayed();
    }

    public boolean isModalTitleDisplayed() {
        return modalTitle.exists() && modalTitle.isDisplayed();
    }

    /**
     * Actions
     */
    public void openHelpModal() {
        questionIcon.click();
    }

}


