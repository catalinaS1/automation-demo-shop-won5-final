package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class BasePage {
    private final SelenideElement modalCloseButton = $(".close");
    private final SelenideElement refresh = $("[data-icon=undo]");

    public void refresh() {
        Selenide.refresh();
    }

    public void resetApp() {
        if (modalCloseButton.exists())
            modalCloseButton.click();
        if (refresh.exists())
            refresh.scrollIntoView(true);
            refresh.click();
    }

    public void closeModal() {
        modalCloseButton.click();
    }
}

