package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.String.format;

public class Product extends BasePage {
    private final SelenideElement title;
    private final SelenideElement price;
    private final SelenideElement mainProductCard;
    private final SelenideElement favoritesButton;

    private final String expectedUrl;
    private final String productId;
    private final String expectedPrice;


    public Product(String productId, String price) {
        this.productId = productId;
        this.expectedPrice = price;
        this.title = $(format("[href='#/product/%s']", productId));
        this.price = title.parent().parent().$(".card-footer .card-text");
        this.expectedUrl = format(DataSource.HOME_PAGE+ "#/product/%s",productId);
        this.mainProductCard = title.parent().parent();
        this.favoritesButton = mainProductCard.$("[data-icon*=heart]");
    }

    public String getProductId() {
        return productId;
    }

    public String getTitle() {
        return title.getText();
    }

    public boolean isDisplayed() {
        return title.exists() && title.isDisplayed();
    }

    public String url() {
        return title.getAttribute("href");
    }

    public String getExpectedUrl() {
        return this.expectedUrl;
    }

    public String getExpectedPrice() {
        return this.expectedPrice;
    }

    public String getPrice() {
        return this.price.getText();
    }

    public void addToBasket() {
        SelenideElement parent = title.parent().parent();
        SelenideElement addToBasketIcon = parent.$("[data-icon=cart-plus]");
        addToBasketIcon.click();
    }

    public void addToWishlist() {
        favoritesButton.click();
    }

    public boolean isInWishList() {
        return favoritesButton.getAttribute("data-icon").equals("heart-broken");
    }
}
