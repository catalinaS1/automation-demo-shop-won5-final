
# Demo Shop Test Automation Framework
A brief presentation of my project

### This is the final project for Catalina, within the FastTrackIt Test Automation course.

### Software engineer: *Catalina Sirghi*

### Tech stack used:
    - Java17
    - Maven
    - Selenide Framwork
    - PageObject Models

### How to run the tests
`git clone https://gitlab.com/catalinaS1/automation-demo-shop-won5-final.git`

#### Execute all tests
- `mvn clean tests`
#### Generate Report
- `mvn allure:report`
- `mvn allure:serve`
> Open and present report

#### Page Objects
    - HomePage
    - Header
    - Footer
    -CartPage
    - Body / Products

### Tests implemented

#### Home page test suite

| **Test Name**                  | **Test Description**                                                                                | **Test Status** |
|:-------------------------------|:----------------------------------------------------------------------------------------------------|:---------------:|
| Verify greetings message       | Verify if greetings message is displayed on home page and if <br/>the message is "Hello guest!"     |    **PASS**     |
| Verify home page logo          | Verify if page logo is displayed and if clicking on logo redirects to home page.                    |    **PASS**     |
| Verify home page title         | Verify if home page title is displayed  and if the title is "Products"                              |    **PASS**     |
| Verify cart icon               | Verify if cart icon is displayed on header and if clicking on it redirects to cart page             |    **PASS**     |
| Verify wishlist icon           | Verify that wishlist icon is displayed on header and that clicking on it redirects to wishlist page |    **PASS**     |
| Verify login button            | Verify if login button is displayed on header and if clicking on it opens the login modal           |    **PASS**     |
| Verify search form             | Verify if search field and search button are displayed                                              |    **PASS**     |
| Verify searching functionality | Verify searching functionality by filling the search field with product's names available on page   |    **PASS**     |
| Verify sort mode products      | Verify if sort mode is displayed on home page and if clicking it opens cascading sort form          |    **PASS**     |

#### Cart page test suite

| **Test Name**                   | **Test Description**                                                                     | **Test Status** |
|:--------------------------------|:-----------------------------------------------------------------------------------------|:---------------:|
| Verify cart page title          | Verify if the title of the cart page is "Your cart"                                      |    **PASS**     |
| Verify cart page message        | Verify if the message of the cart page is "How about adding some products in your cart?" |    **PASS**     |
| Verify continue shopping button | Verify if clicking on continue shopping button redirects to product page                 |    **PASS**     |

#### Login test suite

| **Test Name**              | **Test Description**                                                                                       | **Test Status** |
|:---------------------------|:-----------------------------------------------------------------------------------------------------------|:---------------:|
| Verify username field      | Verify that, after you open the login modal, a username field exists                                       |    **PASS**     |
| Verify password field      | Verify that, after you open the login modal, a password field exists                                       |    **PASS**     |
| Verify submit button       | Verify that, after you open the login modal, a submit button exists                                        |    **PASS**     |
| Verify login functionality | Verify that, if you enter valid credentials in username and password fields, you can login successfully    |    **PASS**     |
| Verify logout button       | Verify that, if logged in with valid credentials, the logout button is displayed and it signs out the user |    **PASS**     |

#### Demo shop test suite

| **Test Name**                      | **Test Description**                                                                              | **Test status** |
|:-----------------------------------|:--------------------------------------------------------------------------------------------------|:---------------:|
| Verify cart badge                  | Verify if the cart icon badge is displayed after adding a product to cart                         |    **PASS**     |
| Verify cart badge number           | Verify if the cart icon badge updates upon adding a product to cart                               |    **PASS**     |
| Verify add to favorites            | After adding a product to favorites, the heart icon under the product crashes                     |    **PASS**     |
| Verify favorites badge number      | Verify if the favourites icon badge updates upon adding a product to wishlist                     |    **PASS**     |
| Verify products                    | Verify if all products are displayed on page and if the price and redirecting url are as expected |    **PASS**     |
| Verify question icon functionality | Verify if question icon is displayed in the footer and if clicking it opens help modal            |    **PASS**     |
| Verify help modal title            | Verify if the title of the help modal is "Help"                                                   |    **PASS**     |

**Reporting is also available in the Allure-results folder**
         _tests are executed using maven_





